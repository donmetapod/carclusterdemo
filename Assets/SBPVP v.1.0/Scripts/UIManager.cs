﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	[SerializeField] GameObject car;
	[SerializeField] CarControlCS carInfo;
	[SerializeField] Text currentSpeed;
	[SerializeField] Text throttle;
	[SerializeField] Text rpm;
	[SerializeField] Image dial;
	[SerializeField] Image pointer;
	[SerializeField] GameObject pointer3D;
	[SerializeField] GameObject pointer3DRPM;
	[SerializeField] Image rpmPointer;
	[SerializeField] Transform[] UICarWheels;
	[SerializeField] Image speedRing;
	[SerializeField] Image rpmRing;
	[SerializeField] Color backgroundColor;
	[SerializeField] AudioClip[] songs;
	[SerializeField] Image[] bars;
	[SerializeField] Transform[] carTransforms;
	[SerializeField] Slider slider;
	[SerializeField] Image sliderFill;
	[SerializeField] GameObject[] carParts;

	public enum ClusterAnimation {Minimalist, Expand, NoRotation, Rotating}
	public ClusterAnimation clusterAnimation;

	[Header("Animations")]
	[SerializeField] Animator[] anims;

	int currentPosition = 0;
	bool moving;
	bool rotate;
	bool introFinished;

	// Use this for initialization
	void Start () {
		ChangeBackgroundColor (backgroundColor);	
		RuntimeAnimatorController ac = anims [(int)clusterAnimation].runtimeAnimatorController;
		if (ac.animationClips [0].name == "Take 001") {
			print ("length " +  ac.animationClips[0].length);
			Invoke ("StartUpdating", ac.animationClips[0].length+3);
			anims [(int)clusterAnimation].gameObject.SetActive (true);
			anims [(int)clusterAnimation].SetTrigger ("Intro");
		}
		for (int i = 0; i < bars.Length; i++) {
			bars [i].rectTransform.localPosition = new Vector3 (bars [i].rectTransform.localPosition.x + (i * 5), bars [i].rectTransform.localPosition.y, bars [i].rectTransform.localPosition.z);
			bars [i].rectTransform.localScale = new Vector3(bars [i].rectTransform.localScale.x,0,bars [i].rectTransform.localScale.z);
		}
	}
	
	void StartUpdating(){
		anims [(int)clusterAnimation].enabled = false;
		StartCoroutine (MoveToPosition (car, carTransforms [currentPosition].position));
		introFinished = true;
	}

	// Update is called once per frame
	void Update () {
		if (!introFinished) {
			return;
		}
		float speed = Mathf.Floor (carInfo.currentSpeed);
		currentSpeed.text = speed.ToString();
		throttle.text = "Throttle: " + carInfo.throttleInput.ToString ();
		float r = carInfo.RPM * 5000;
		rpm.text = "RPM: " + r.ToString ();
		foreach (Transform item in UICarWheels) {
			item.Rotate (carInfo.currentSpeed, 0, 0);
		}
		if(!carInfo.reversing){
			Quaternion targetSpeedRot = Quaternion.Euler(pointer.GetComponent<RectTransform>().localRotation.x, pointer.GetComponent<RectTransform>().localRotation.y, -carInfo.currentSpeed);
			pointer.GetComponent<RectTransform>().localRotation = Quaternion.Slerp(pointer.GetComponent<RectTransform>().rotation, targetSpeedRot, 1);

			Quaternion targetRPMdRot = Quaternion.Euler(0, 0, -carInfo.RPM*100);
			rpmPointer.GetComponent<RectTransform> ().localRotation = Quaternion.Slerp(rpmPointer.GetComponent<RectTransform>().rotation, targetRPMdRot, 1);

			//Quaternion target3DSpeedRot = Quaternion.Euler (pointer3D.transform.localRotation.x, pointer3D.transform.localRotation.y, -carInfo.currentSpeed);
			Quaternion target3DSpeedRot = Quaternion.Euler (  pointer3D.transform.localRotation.x, -carInfo.currentSpeed, pointer3D.transform.localRotation.z);
			pointer3D.transform.localRotation = Quaternion.Slerp (pointer3D.transform.rotation, target3DSpeedRot, 1);

			Quaternion target3DRPMRot = Quaternion.Euler (  pointer3DRPM.transform.localRotation.x, carInfo.RPM*100, pointer3DRPM.transform.localRotation.z);
			pointer3DRPM.transform.localRotation = Quaternion.Slerp (pointer3DRPM.transform.rotation, target3DRPMRot, 1);
		}

//		speedRing.color = new Color (carInfo.currentSpeed, speedRing.color.g, speedRing.color.b, speedRing.color.a);
//		rpmRing.color = new Color (carInfo.RPM, speedRing.color.g, speedRing.color.b, speedRing.color.a);
//		unispeedRing.color = new Color (speedRing.color.r, speedRing.color.g, speedRing.color.b, (carInfo.currentSpeed/255));
//		rpmRing.color = new Color (rpmRing.color.r, rpmRing.color.g, rpmRing.color.b, (carInfo.RPM/5));


		float[] spectrum = new float[256];

		AudioListener.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);

		for (int i = 0; i < bars.Length; i++) {
			bars [i].rectTransform.localScale = new Vector3(bars [i].rectTransform.localScale.x, spectrum [i]);
		}

		if (rotate) {
			car.transform.Rotate (0, 1, 0);
		}

		slider.value = carInfo.RPM*0.65f;
		sliderFill.color = new Color(carInfo.RPM, 1-carInfo.RPM, sliderFill.color.b, sliderFill.color.a);	

	}

	void ChangeBackgroundColor(Color targetColor){
		Camera.main.backgroundColor = targetColor;
	}

	public void OnButtonPressed(string button){
		switch (button) {
		case "PlaySong":
			int rnd = Random.Range (0, songs.Length);
			Camera.main.GetComponent<AudioSource> ().clip = songs [rnd];
			Camera.main.GetComponent<AudioSource> ().Play ();
			break;
		case "ChangeZoom":
			if (!moving) {
				switch (currentPosition) {
				case 0:
					currentPosition++;
					StartCoroutine (MoveToPosition (car, carTransforms [currentPosition].position));
					break;
				case 1:
					currentPosition++;
					StartCoroutine (MoveToPosition (car, carTransforms [currentPosition].position));
					break;
				case 2:
					currentPosition = 0;
					StartCoroutine (MoveToPosition (car, carTransforms [currentPosition].position));
					break;
				}
			}
			break;
		}

	}
		
	IEnumerator MoveToPosition(GameObject targetObject, Vector3 targetPos){
		moving = true;
		while (Vector3.Distance (targetObject.transform.position, targetPos) > 0.1f) {
			targetObject.transform.position = Vector3.Lerp (targetObject.transform.position, targetPos, Time.deltaTime * 5);
			yield return new WaitForEndOfFrame ();
		}

		targetObject.transform.position = targetPos;
		moving = false;
	}

	public void Rotate(){
		rotate = true;
	}

	public void StopRotating(){
		rotate = false;
	}
}
